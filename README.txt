/---- USAGE ----/
Simply upload the drupal-check.php file to the server and navigate to it in your browser.

Assuming PHP is switched on, you will quickly know if the environment can handle your Drupal 6 or 7 install.


/---- DETAILS ----/
Requirements have been based on the information on drupal.org:
http://drupal.org/requirements
